<?php 
	
	add_theme_support( 'menus' );

	function register_custom_menu() {
		register_nav_menus(
			array(
				"primary-menu" => __( "Primary Menu"),
				"secundary-menu" => __( "Secundary Menu")
			)
		);

	}

	add_action( "init", "register_custom_menu" );

	function my_style(){
		wp_enqueue_style( "bootstrap", get_template_directory_uri() . "/js/bootstrap/css/bootstrap.css" );
		wp_enqueue_style( "main", get_template_directory_uri() . "/style.css" );

	}

	add_action( "wp_enqueue_scripts", "my_style" );

	function my_script(){
		
		//wp_enqueue_script( "nesto_js", get_template_directory_uri() . "/js/nesto.js", "", "",false );
		wp_enqueue_script( "bootstrap_min", get_template_directory_uri() . "/js/bootstrap/js/bootstrap.min.js", array("jquery"), "",true );
		wp_enqueue_script( "custom_js", get_template_directory_uri() . "/js/custom.js", array("jquery"), "", true );
		
	}

	add_action( "wp_enqueue_scripts", "my_script" );
	
	include("/registration-and-profile/init.php");

?>



