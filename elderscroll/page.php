
<?php get_header(); ?>
	 
<section class="main-content">
	<div class="row">
		<div class="container">
			
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>

			
			
		</div>
	</div>
</section>
		
<?php get_footer(); ?>
