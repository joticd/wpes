<!DOCTYPE html>
<html lang="no">
    <head>
    	<meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">        
        <title><?php wp_title(); ?></title>
        <meta name="robots" content="noindex, nofollow">
        <link rel="icon" type="image/x-icon" href="/images/favicon.ico" />
        
        <!-- stylesheets -->
        
        <!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="../stylesheets/ie8.css" /><![endif]-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php wp_head(); ?>

    </head>
    
        <?php 
            $defaults = array(
                "container" => false,
                "theme_location" => "primary-menu",
                "menu_id" => "top-menu-id"
            );
        ?>
        <body>

            <section class="header-container">
                <header>
                    <div class="row">
                        <div class="container">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="logo">
                                    <a href="#"><div id="logo"></div></a>
                                </div>
                                <div class="top-menu">
                                    <button class="navbar-toggle" data-toggle="collapse">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <?php wp_nav_menu($defaults); ?>  
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </section>
        
    
    
        

       