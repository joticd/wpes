
<?php 
    $menuFooter = array(
        "container" => false,
        "theme_location" => "secundary-menu",
        "menu_id" => "footer-menu-id"
    );
?>

<section class="footer-section">
            <footer>
                <div class="row">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="footer-menu">
                                <?php wp_nav_menu($menuFooter); ?> 
                                <p>&copy; Red Satchel Pty Ltd 2014</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="twitter-title">
                                <h3>follow us on twitter</h3>
                                <a href="#"><img src="/images/twitter-image.png" alt=""></a>
                            </div>
                            <div class="twitter-posts">
                                THIS IS THE PLACE FOR TWITTER POSTS!
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>

    	<?php wp_footer(); ?>
    </body>
</html>

