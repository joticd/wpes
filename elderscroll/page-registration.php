<?php
/*
Template Name: page-registration
 */
?>



<?php get_header(); ?>
<div class="gray-background">
    <div class="container">
        <p>eldercareWISE User Registration</p>
    </div>
</div>	 
<section class="main-content registration-page">
	<div class="row">
		<div class="container">
			
					
					<?php $sp_regform_standard->display_form(); ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
					
		</div>
	</div>
</section>
		
<?php get_footer(); ?>
